<?php

namespace Backend\ApiRestBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Groups;

use Doctrine\ORM\Mapping as ORM;

/**
 * Domicilio
 *
 * @ORM\Table(name="Domicilio")
 * @ORM\Entity(repositoryClass="Backend\ApiRestBundle\Repository\DomicilioRepository")
 */
class Domicilio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @Groups({"list_pedido"})
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(message = "Los datos de la dirección son requeridos")
     *
     * @ORM\Column(name="direccion", type="string", length=255, nullable=false)
     *
     * @Groups({"tareas_pendientes","list_pedido"})
     */
    private $direccion;

    /**
     * @var \Backend\ApiRestBundle\Entity\Cliente
     * @Assert\NotBlank(message = "Cliente es requerido")
     *
     * @ORM\ManyToOne(targetEntity="Backend\ApiRestBundle\Entity\Cliente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cliente", referencedColumnName="id", nullable=false)
     * })
     *
     * @Groups({"tareas_pendientes","list_pedido"})
     */
    private $cliente;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Domicilio
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set cliente
     *
     * @param \Backend\ApiRestBundle\Entity\Cliente $cliente
     *
     * @return Domicilio
     */
    public function setCliente(\Backend\ApiRestBundle\Entity\Cliente $cliente = null)
    {
        $this->cliente = $cliente;

        return $this;
    }

    /**
     * Get cliente
     *
     * @return \Backend\ApiRestBundle\Entity\Cliente
     */
    public function getCliente()
    {
        return $this->cliente;
    }
}
