<?php

namespace Backend\ApiRestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Backend\ApiRestBundle\Entity\Domicilio;
use Backend\ApiRestBundle\Entity\Pedido;

use FOS\RestBundle\Context\Context;


class PedidoController extends FOSRestController
{

    private $errors = array();

    /**
     * @Rest\Get("/all")
     */
    public function getAction()
    {
        $restresult = $this->getDoctrine()->getRepository('BackendApiRestBundle:Pedido')->findAll();
        if ($restresult === null) {
            return new View("No hay pedidos", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * Almacena un pedido y todas las entidades que lo relacionan
     * Si vine en el request un email de un cliente existente, no se almacenan los datos del usuario
     * Si vine en el request el id_domicilio, trabajamos con esete y no creamos registro de Domicilio
     *
     * @Rest\Post("/guardar")
     */
    public function postAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $serializer = $this->container->get('jms_serializer');
        $cliente_ns = 'Backend\ApiRestBundle\Entity\Cliente';
        $cliente = $request->get('cliente');

        //Comprobamos si el usuario ya existe
        if (empty($request->get('cliente'))) return new View("CLIENTE NO ENCONTRADO", Response::HTTP_NOT_FOUND);

        $clienteObj = $em->getRepository('BackendApiRestBundle:Cliente')->findOneBy(array('email' => $cliente['email']));
        if (!$clienteObj) $clienteObj = $serializer->deserialize( json_encode($cliente), $cliente_ns, 'json');

        //Si viene algún id domicilio pero este no concuerda con el cliente, se anula y se toman los datos del domicilio
        $domicilioObj = null;
        if (!empty($request->get('id_domicilio'))){
            $domicilioObj = $em->getRepository('BackendApiRestBundle:Domicilio')->find($request->get('id_domicilio'));
            if ($domicilioObj && $domicilioObj->getCliente()->getId() != $clienteObj->getId()) $domicilioObj = null;
        }

        if (!$domicilioObj) {
            $domicilioObj = new Domicilio();
            $domicilioObj->setCliente($clienteObj);
            $domicilioObj->setDireccion($request->get('direccion'));
        }

        $pedidoObj = new Pedido();
        $pedidoObj->setDomicilio($domicilioObj);
        $pedidoObj->setFechaEntrega(\DateTime::createFromFormat('Y-m-d', $request->get('fecha_entrega')));
        $pedidoObj->setFranjaHora($request->get('franja_hora'));
        $pedidoObj->setDriver($this->get('Extras')->getRandomObjDriver());

        $validator = $this->get('validator');
        if (count($validator->validate($clienteObj))) $this->errors[] = $validator->validate($clienteObj);
        else if (count($validator->validate($domicilioObj))) $this->errors[] = $validator->validate($domicilioObj);
        else if (count($validator->validate($pedidoObj))) $this->errors[] = $validator->validate($pedidoObj);

        if (count($this->errors)) {
            foreach ($this->errors as $linea) {
                foreach ($linea as $error) $this->msgError[] = $error->getMessage();
            }
            $errors['errors'] = $this->msgError;
            return new View($errors, Response::HTTP_NOT_ACCEPTABLE);
        } else {
            try {
                $em->persist($clienteObj);
                $em->persist($domicilioObj);
                $em->persist($pedidoObj);
                $em->flush();

                return new View("PEDIDO CREADO", Response::HTTP_OK);

            } catch (\Doctrine\DBAL\DBALException $e){
                return new View($e, Response::HTTP_NOT_ACCEPTABLE);
            }
        }
    }


    /**
     * Método que devuelve todas las tareas pendientes de un Driver según fecha indicada
     *
     * @Rest\Get("/pendientes/{id_driver}/{fecha_entrega}",
     *     requirements={"id_driver" = "\d+"},
     *     defaults={"id" = null, "fecha_entrega" = null}
     *     )
     */
    public function pendientesAction($id_driver, $fecha_entrega)
    {
        if ($id_driver && $fecha_entrega) {
            $valores = explode('-', $fecha_entrega);
            if (!checkdate($valores[1], $valores[2], $valores[0]))
                return new View("Solo se admine el fomrato de fecha 'yyyy-mm-dd'", Response::HTTP_BAD_REQUEST);

            $dql = $this->getDoctrine()
                ->getRepository('BackendApiRestBundle:Pedido')
                ->createQueryBuilder('P')
                ->select("P");

            $dql->andWhere('P.driver = :driver_id')
                ->setParameter('driver_id', $id_driver);

            $dql->andWhere('YEAR(P.fechaEntrega) = :year')->setParameter('year', $valores[0])
                ->andWhere('MONTH(P.fechaEntrega) = :month')->setParameter('month', $valores[1])
                ->andWhere('DAY(P.fechaEntrega) = :day')->setParameter('day', $valores[2]);

            $dql->andWhere("P.hecho = false")
                ->orderBy('P.fechaEntrega','ASC');

            $result = $dql->getQuery()->getResult();

            if ($result === null) return new View("Tareas no encontradas", Response::HTTP_NOT_FOUND);

            $view = $this->view($result, Response::HTTP_OK);
            $context = new Context();
            $context->addGroup('tareas_pendientes');
            $view->setContext($context);

            return $this->handleView($view);

        } else return new View("valores no permitidos", Response::HTTP_NOT_ACCEPTABLE);

    }

}
